public class SalariedEmployee extends Employee {
    private double weeklySalary;

    public SalariedEmployee() {
    }

    public SalariedEmployee(String firstName, String lastName, String securityNumber, double weeklySalary) {
        super(firstName, lastName, securityNumber);
        this.weeklySalary = weeklySalary;
    }

    public double getWeeklySalary() {
        return weeklySalary;
    }

    public void setWeeklySalary(double weeklySalary) {
        this.weeklySalary = weeklySalary;
    }

    @Override
    public double getPaymentAmount() {
        return getWeeklySalary() * 4;
    }

    @Override
    public String toString() {
        return "SalariedEmployee{" +
                super.toString() +
                "weeklySalary=" + weeklySalary +
                '}';
    }
}
