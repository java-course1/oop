public class Invoice implements Payable {
    private String partNumber;
    private String partDescription;
    private int qty;
    private double pricePerItem;

    public Invoice(String partNumber, String partDescription, int qty, double pricePerItem) {
        this.partNumber = partNumber;
        this.partDescription = partDescription;
        this.qty = qty;
        this.pricePerItem = pricePerItem;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getPartDescription() {
        return partDescription;
    }

    public void setPartDescription(String partDescription) {
        this.partDescription = partDescription;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getPricePerItem() {
        return pricePerItem;
    }

    public void setPricePerItem(double pricePerItem) {
        this.pricePerItem = pricePerItem;
    }

    @Override
    public double getPaymentAmount() {
//        return qty * pricePerItem;
        return getQty() * getPricePerItem();
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "partNumber='" + partNumber + '\'' +
                ", partDescription='" + partDescription + '\'' +
                ", qty=" + qty +
                ", pricePerItem=" + pricePerItem +
                ", payment=" + getPaymentAmount() +
                '}';
    }
}
