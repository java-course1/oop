public class CommissionEmployee extends Employee {
    private double grossSale;
    private double commissionRate;

    public CommissionEmployee() {
    }

    public CommissionEmployee(String firstName, String lastName, String securityNumber, double grossSale, double commissionRate) {
        super(firstName, lastName, securityNumber);
        this.grossSale = grossSale;
        this.commissionRate = commissionRate;
    }

    public double getGrossSale() {
        return grossSale;
    }

    public void setGrossSale(double grossSale) {
        this.grossSale = grossSale;
    }

    public double getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(double commissionRate) {
        this.commissionRate = commissionRate;
    }

    @Override
    public double getPaymentAmount() {
        return getGrossSale() * getCommissionRate();
    }

    @Override
    public String toString() {
        return "CommissionEmployee{" +
                "grossSale=" + grossSale +
                ", commissionRate=" + commissionRate +
                '}';
    }
}
