public class HourlyEmployee extends Employee {
    private double wage;
    private double hours;

    public HourlyEmployee() {
    }

    public HourlyEmployee(String firstName, String lastName, String securityNumber, double wage, double hours) {
        super(firstName, lastName, securityNumber);
        this.wage = wage;
        this.hours = hours;
    }

    public double getWage() {
        return wage;
    }

    public void setWage(double wage) {
        this.wage = wage;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }

    @Override
    public double getPaymentAmount() {
        return getWage() * getHours();
    }
}
